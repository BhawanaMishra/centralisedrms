Collision no: 1
Train 1: 12350
Train 2: 12351
Collision Type: REAREND
The collision can be avoided.
Train 12350: MOVE
Train 12351: STOP

Collision no: 2
Train 1: 12352
Train 2: 12353
Collision Type: REAREND
The collision can be avoided.
Train 12352: MOVE
Train 12353: STOP

Collision no: 3
Train 1: 12354
Train 2: 12355
Collision Type: REAREND
The collision can be avoided.
Train 12354: STANDING
Train 12355: STOP

Collision no: 4
Train 1: 12356
Train 2: 12357
Collision Type: HEADON
The collision can be avoided.
Train 12356: MOVE
Train 12357: STOP

Collision no: 5
Train 1: 12358
Train 2: 12360
Collision Type: HEADON
The collision can be avoided.
Train 12358: MOVE
Train 12360: STOP

Collision no: 6
Train 1: 12359
Train 2: 12361
Collision Type: HEADON
The collision can be avoided.
Train 12359: MOVE
Train 12361: STOP

Collision no: 7
Train 1: 12363
Train 2: 12364
Collision Type: REAREND
The collision can be avoided.
Train 12363: MOVE
Train 12364: STOP

Collision no: 8
Train 1: 12365
Train 2: 12366
Collision Type: HEADON
The collision can be avoided.
Train 12365: MOVE
Train 12366: STOP

Collision no: 9
Train 1: 12369
Train 2: 12370
Collision Type: REAREND
The collision can be avoided.
Train 12369: MOVE
Train 12370: STOP

Collision no: 10
Train 1: 12372
Train 2: 12373
Collision Type: HEADON
The collision can be avoided.
Train 12372: MOVE
Train 12373: STOP


-----------------
Total no of collisions: 10
Total no of avoidance: 10
