/*
Arguments to create a TrainAgent
name
track
//dir
station from
station to
time
coordinates
*/
package TrainManagement;

import jade.core.Agent;
//import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.core.AID;
import jade.core.behaviours.CyclicBehaviour;
//import jade.core.behaviours.WakerBehaviour;
import java.util.Scanner;
import java.io.*;

public class TrainAgent extends Agent {
    
    String name;
    int track;
    int dir;
    String stationFrom;
    String stationTo;
    String time;
    String coordinates;
    double velocity;
    double retard;
    String path[];
    String stationToCoordinates;
    String stationFromCoordinates;
    int priority;
   
    String Msg;
    Scanner in = new Scanner(System.in);
    static int count = 0;
    
    protected void setup() {
        // receiving messages from First
        ACLMessage msg=new ACLMessage(ACLMessage.REQUEST);
        //Object[] args = getArguments();
        String args[]=null;
        
        try{
            File dir = new File(".");
            File fin = new File(dir.getCanonicalPath() + File.separator + "Dataset/Train/TrainFinal.txt");		
            FileInputStream fis = new FileInputStream(fin);
            //Construct BufferedReader from InputStreamReader
            BufferedReader br = new BufferedReader(new InputStreamReader(fis));
            String line;
            int i=0;
            
            while ((line = br.readLine()) != null && i<=count) {
                //System.out.println(line);
                args = line.split(" ");
                i++;
            }
            System.out.println(line+"----Length="+args.length);
        }catch(IOException e){
            System.out.println("Train Agent cannot be created!! "+e);
        }
        
        name = args[0];
        track = Integer.parseInt(args[4]);
        dir = Integer.parseInt(args[5]);
        stationTo = args[2];
        time = args[1];
        coordinates = args[3];
        velocity = Double.parseDouble(args[6]);
        retard = Double.parseDouble(args[7]);
        path = args[8].substring(1,args[8].length()-1).split(",");
        stationToCoordinates = args[9];
        stationFromCoordinates = args[10];
        priority = Integer.parseInt(args[11]);
        
        int i;
        if(dir==1){
            for(i=0; i<path.length;i++){
                if(path[i].equals(stationTo))
                    break;
            }
            stationFrom = path[i-1];
        }else{
            for(i=path.length-1; i>=0;i--){
                if(path[i].equals(stationTo))
                    break;
            }
            stationFrom = path[i+1];
        }
        
        count++;
        String abc;
        
        String To,From;
        To = stationTo;
        From = stationFrom;
        
        //source message passing
        if(stationFrom.charAt(0) == 'j'){
            if(dir == 1){
                for(i=0 ; i<path.length ; i++)
                    if(path[i].equals(stationFrom))
                        break;
                
                for(; i>=0 ; i--)
                    if(path[i].charAt(0) == 's')
                        break;
            }
            else{
                for(i=path.length-1 ; i>=0 ; i--)
                    if(path[i].equals(stationFrom))
                        break;
                
                for(; i<path.length ; i++)
                    if(path[i].charAt(0) == 's')
                        break;
            }
            
            From = path[i];
        }
        msg.clearAllReceiver();
        abc = name+","+track+","+2+","+time+","+coordinates+","+velocity+","+stationFrom+","+stationTo+","+stationToCoordinates+","+stationFromCoordinates+","+retard+","+priority;
        msg.setContent(abc);
        msg.addReceiver(new AID(From,AID.ISLOCALNAME));
        send(msg);
        
        
        //destination message passing
        if(stationTo.charAt(0) == 'j'){
            if(dir == 1){
                for(i=0 ; i<path.length ; i++)
                    if(path[i].equals(stationTo))
                        break;
                
                for(; i<path.length ; i++)
                    if(path[i].charAt(0) == 's')
                        break;
            }
            else{
                for(i=path.length-1 ; i>=0 ; i--)
                    if(path[i].equals(stationTo))
                        break;
                
                for(; i>=0 ; i--)
                    if(path[i].charAt(0) == 's')
                        break;
            }
            
            To = path[i];
        }
        
        msg.clearAllReceiver();
        abc = name+","+track+","+1+","+time+","+coordinates+","+velocity+","+stationFrom+","+stationTo+","+stationToCoordinates+","+stationFromCoordinates+","+retard+","+priority;
        msg.setContent(abc);
        msg.addReceiver(new AID(To,AID.ISLOCALNAME));
        send(msg);
        
        addBehaviour(new CyclicBehaviour(this) {
            
            MessageTemplate mt=MessageTemplate.MatchPerformative(ACLMessage.INFORM);
            @Override
            public void action() {
                
                ACLMessage msg1=receive(mt);
                
                if(msg1!=null) {
                    msg1.clearAllReceiver();
                    Msg=msg1.getContent();
                    AID sen= msg1.getSender();
                    
                   System.out.println(getAID().getLocalName()+": Message received from station "+sen.getLocalName()+"\n\t"+Msg);
                    
                }else{
                    //System.out.println("No got!!");
                    //block();
                }
            }
        });
    }
}
